/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// MyAppResourceSpec defines the desired state of MyAppResource
type MyAppResourceSpec struct {
	ReplicaCount int32 `json:"replicaCount"`

	Resources ResourceRequirements `json:"resources"`

	Image ImageSpec `json:"image"`

	UI UIConfig `json:"ui"`

	Redis RedisSpec `json:"redis"`
}

// ResourceRequirements defines the resources required by the MyAppResource
type ResourceRequirements struct {
	MemoryLimit string `json:"memoryLimit"`
	CPURequest  string `json:"cpuRequest"`
}

// ImageSpec defines the image specification for the MyAppResource
type ImageSpec struct {
	Repository string `json:"repository"`
	Tag        string `json:"tag"`
}

// UIConfig defines the UI configuration for the MyAppResource
type UIConfig struct {
	Color   string `json:"color"`
	Message string `json:"message"`
}

// RedisSpec defines the Redis configuration for the MyAppResource
type RedisSpec struct {
	Enabled bool `json:"enabled"`
}

// MyAppResourceStatus defines the observed state of MyAppResource
type MyAppResourceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	AvailableReplicas int32 `json:"availableReplicas"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// MyAppResource is the Schema for the myappresources API
type MyAppResource struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   MyAppResourceSpec   `json:"spec,omitempty"`
	Status MyAppResourceStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// MyAppResourceList contains a list of MyAppResource
type MyAppResourceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []MyAppResource `json:"items"`
}

func init() {
	SchemeBuilder.Register(&MyAppResource{}, &MyAppResourceList{})
}
