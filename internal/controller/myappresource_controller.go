/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"reflect"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/intstr"
	"my.api.group/myapp-controller/api/v1alpha1"
	myapigroupv1alpha1 "my.api.group/myapp-controller/api/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// MyAppResourceReconciler reconciles a MyAppResource object
type MyAppResourceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=my.api.group,resources=myappresources,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=my.api.group,resources=myappresources/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=my.api.group,resources=myappresources/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups="",resources="",verbs=get;list;watch;create;update;patch;delete

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.0/pkg/reconcile

func (r *MyAppResourceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := ctrl.Log.WithValues("myappresource", req.NamespacedName)

	// Fetch the MyAppResource instance
	myAppResource := &myapigroupv1alpha1.MyAppResource{}
	err := r.Get(ctx, req.NamespacedName, myAppResource)
	if err != nil {
		log.Error(err, "Failed to get MyAppResource")
		return reconcile.Result{}, err
	}

	// create a Deployment with the specified replica count and resources

	deployment := &appsv1.Deployment{}
	err = r.Get(ctx, types.NamespacedName{Name: myAppResource.Name, Namespace: myAppResource.Namespace}, deployment)
	if err != nil && errors.IsNotFound(err) {
		// Deployment doesn't exist, create a new one
		log.Info("Creating Deployment", "Namespace", myAppResource.Namespace, "Name", myAppResource.Name)
		deployment = &appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      myAppResource.Name,
				Namespace: myAppResource.Namespace,
				Labels: map[string]string{
					"app": "myappresource",
				},
			},
			Spec: appsv1.DeploymentSpec{
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{
						"app": "myappresource",
					},
				},
				Replicas: &myAppResource.Spec.ReplicaCount,
				Template: corev1.PodTemplateSpec{
					ObjectMeta: metav1.ObjectMeta{
						Labels: map[string]string{
							"app": "myappresource",
						},
					},
					Spec: corev1.PodSpec{
						Containers: []corev1.Container{
							{
								Name:  "myapp",
								Image: fmt.Sprintf("%s:%s", myAppResource.Spec.Image.Repository, myAppResource.Spec.Image.Tag),
								Resources: corev1.ResourceRequirements{
									Limits: corev1.ResourceList{
										corev1.ResourceMemory: resource.MustParse(myAppResource.Spec.Resources.MemoryLimit),
									},
									Requests: corev1.ResourceList{
										corev1.ResourceCPU: resource.MustParse(myAppResource.Spec.Resources.CPURequest),
									},
								},
								Env: []corev1.EnvVar{
									{
										Name:  "PODINFO_UI_COLOR",
										Value: myAppResource.Spec.UI.Color,
									},
									{
										Name:  "PODINFO_UI_MESSAGE",
										Value: myAppResource.Spec.UI.Message,
									},
									{
										Name:  "PODINFO_CACHE_SERVER",
										Value: fmt.Sprintf("tcp://%s:%d", "localhost", 6379),
									},
								},
							},
						},
					},
				},
			},
		}

		// Add Redis container and service if enabled
		if !reflect.DeepEqual(myAppResource.Spec.Redis, v1alpha1.RedisSpec{}) && myAppResource.Spec.Redis.Enabled {
			// Add Redis container to the pod spec
			deployment.Spec.Template.Spec.Containers = append(deployment.Spec.Template.Spec.Containers, corev1.Container{
				Name:  "redis",
				Image: "redis:latest",
				Resources: corev1.ResourceRequirements{
					Limits: corev1.ResourceList{
						corev1.ResourceMemory: resource.MustParse("256Mi"), // Example memory limit
						corev1.ResourceCPU:    resource.MustParse("100m"),  // Example CPU limit
					},
					Requests: corev1.ResourceList{
						corev1.ResourceMemory: resource.MustParse("128Mi"), // Example memory request
						corev1.ResourceCPU:    resource.MustParse("50m"),   // Example CPU request
					},
				},
				// Add ports for the Redis container
				Ports: []corev1.ContainerPort{
					{
						Name:          "redis",
						ContainerPort: 6379,               // Redis default port
						Protocol:      corev1.ProtocolTCP, // Protocol TCP
					},
				},
			})
		}

		// Set MyAppResource instance as the owner of the Deployment
		if err := controllerutil.SetControllerReference(myAppResource, deployment, r.Scheme); err != nil {
			log.Error(err, "Failed to set owner reference on Deployment")
			return ctrl.Result{}, err
		}
		// Create the deployment
		if err := r.Create(ctx, deployment); err != nil {
			log.Error(err, "Failed to create Deployment")
			return ctrl.Result{}, err
		}
	} else if err != nil {
		// Error getting the Deployment
		log.Error(err, "Failed to get Deployment")
		return ctrl.Result{}, err
	} else {
		// Deployment exists, update it if needed
		if !reflect.DeepEqual(deployment.Spec.Template.Spec.Containers[0].Image, myAppResource.Spec.Image.Repository+":"+myAppResource.Spec.Image.Tag) ||
			!reflect.DeepEqual(deployment.Spec.Replicas, myAppResource.Spec.ReplicaCount) ||
			!reflect.DeepEqual(deployment.Spec.Template.Spec.Containers[0].Resources, corev1.ResourceRequirements{
				Limits: corev1.ResourceList{
					corev1.ResourceMemory: resource.MustParse(myAppResource.Spec.Resources.MemoryLimit),
				},
				Requests: corev1.ResourceList{
					corev1.ResourceCPU: resource.MustParse(myAppResource.Spec.Resources.CPURequest),
				},
			}) ||
			deployment.Spec.Template.Spec.Containers[0].Env[0].Value != myAppResource.Spec.UI.Color ||
			deployment.Spec.Template.Spec.Containers[0].Env[1].Value != myAppResource.Spec.UI.Message {
			{
				// spec has changed, update the deployment
				log.Info("Updating Deployment", "Namespace", myAppResource.Namespace, "Name", myAppResource.Name)
				deployment.Spec.Replicas = &myAppResource.Spec.ReplicaCount
				deployment.Spec.Template.Spec.Containers[0].Image = myAppResource.Spec.Image.Repository + ":" + myAppResource.Spec.Image.Tag
				deployment.Spec.Template.Spec.Containers[0].Env[0].Value = myAppResource.Spec.UI.Color
				deployment.Spec.Template.Spec.Containers[0].Env[1].Value = myAppResource.Spec.UI.Message
				deployment.Spec.Template.Spec.Containers[0].Resources = corev1.ResourceRequirements{
					Limits: corev1.ResourceList{
						corev1.ResourceMemory: resource.MustParse(myAppResource.Spec.Resources.MemoryLimit),
					},
					Requests: corev1.ResourceList{
						corev1.ResourceCPU: resource.MustParse(myAppResource.Spec.Resources.CPURequest),
					},
				}
				if err := r.Update(ctx, deployment); err != nil {
					log.Error(err, "Failed to update Deployment")
					return ctrl.Result{}, err
				}
			}
		}
	}
	// Create Service for the deployment
	service := &corev1.Service{}
	err = r.Get(ctx, types.NamespacedName{Name: myAppResource.Name + "-service", Namespace: myAppResource.Namespace}, service)
	if err != nil && errors.IsNotFound(err) {
		log.Info("Creating Service", "Namespace", deployment.Namespace, "Name", myAppResource.Name+"-service")
		service = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      myAppResource.Name + "-service",
				Namespace: myAppResource.Namespace,
			},
			Spec: corev1.ServiceSpec{
				Selector: map[string]string{
					"app": "myappresource",
				},
				Ports: []corev1.ServicePort{
					{
						Protocol: corev1.ProtocolTCP,
						Port:     9899,
						TargetPort: intstr.IntOrString{
							IntVal: 9898,
						},
					},
				},
			},
		}
		if err := controllerutil.SetControllerReference(myAppResource, service, r.Scheme); err != nil {
			log.Error(err, "unable to set controller reference for service")
			return ctrl.Result{}, err
		}
		if err := r.Create(ctx, service); err != nil {
			log.Error(err, "unable to create service")
			return ctrl.Result{}, err
		}
	} else if err != nil {
		// Error getting the Service
		log.Error(err, "unable to get service")
		return ctrl.Result{}, err
	} else {
		// Service already exists, do nothing
		log.Info("Service already exists", "name", service.Name)
	}

	return reconcile.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *MyAppResourceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&myapigroupv1alpha1.MyAppResource{}).
		Owns(&appsv1.Deployment{}).
		Complete(r)
}
