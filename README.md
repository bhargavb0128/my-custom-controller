# myapp-controller
Goal is build a custom controller to manage custom resources deployed on the kubernetes cluster. 

## Description
This project aims to build a custom controller to manage custom resources deployed on a Kubernetes cluster. The custom controller watches for changes to custom resources and ensures that the cluster's state matches the desired state specified in those resources.

## Getting Started
To get started with building the custom controller, I chose to use the Kubebuilder framework, which simplifies the process of creating and managing controllers for Kubernetes custom resources.

### Prerequisites
- go version v1.21.0+
- docker version 17.03+.
- kubectl version v1.24.0.
- Kubernetes cluster v1.24.1 (I have used minikue in my case : v1.31.2)
- kubebuilder version v1.27.1 (not required to run the deployment)

### Steps to Deploy the controller on the cluster
```sh
    git clone https://gitlab.com/bhargavb0128/my-custom-controller.git

    cd my-custom-controller

    make deploy IMG=bhargavkb07/mayapp-custom-controller:latest
```


### Steps to verify the controller deployment / CRD, install custom resource and validate the custom resource funtionality
```sh
    kubectl get crds                              

    kubectl get pods -n myapp-controller-system (custom controller deployment)

    kubectl logs pod/controller-podname -n myapp-controller-system (check logs of the controller to verify its ready)

    kubectl apply -f sample.yaml (this is a sample custom resource - verify the controller logs to see the reconciliation kick-off)

    kubectl get pods (check the pod status for custom resource deployment)

    kubectl port-forward svc/service-name 8080:9899 (port-forward on the service as we do not have ingress traffic routing enabled)

```

### Sample deployment and validation
```sh
    make deploy IMG=bhargavkb07/mayapp-custom-controller:latest

    kubectl port-forward svc/myappresource-sample-service 8080:9899

    curl -v -X 'GET' 'http://localhost:8080/'  -H 'accept: text/html'

    curl -v  -X 'POST' 'http://localhost:8080/cache/user=john' -H 'accept: application/json' -d ''

    curl -v 'GET' 'http://localhost:8080/cache/user=john' -H 'accept: application/json'

```


### To Uninstall
**Delete the instances (CRs) from the cluster:**

```sh
    kubectl delete -f sample.yaml
```

**UnDeploy the controller and delete the CRD from the cluster:**

```sh
    make undeploy ignore-not-found=true
```

### Steps to build and push the controller images to the repository in case of changes.

```sh
    make docker-build docker-push IMG=bhargavkb07/mayapp-custom-controller:v0.1.1
```

**NOTE:** This image should be published to the repository before performing the above steps. Since the controller image already exists no need to re-run the build/push unless required. 


### References Used

- [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)

- Used Google references for reconciliation logic


**NOTE:** 
This custom controller has built with basic functionality to addess the current custom resource scenario. However, there is lot of scope for enhancements. And for the sake of simplictiy and ease of deployment process I have added the redis container to the same pod as application and as, we progress in refining the architecture towards production this can be seperated into it's own pod.
#### Enhancement Roadmap:
- [ ] Refine Design/Architecture 
- [ ] CI/CD pipeline
- [ ] Unit Tests
- [ ] Documentation
- [ ] Team Engagement  


